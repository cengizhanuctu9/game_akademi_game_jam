using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnmanager : MonoBehaviour
{
    public GameObject[] birds;
    void Start()
    {
        StartCoroutine(spawn());
    }

   IEnumerator spawn()
    {
        while (true)
        {
            spawnbird();
            yield return new WaitForSeconds(3f);
        }
    }
    void spawnbird()
    {
        int birdscoun = Random.Range(0,4);
        float posy = Random.Range(0, 4.5f);
        Vector3 pos = new Vector3(birds[birdscoun].transform.position.x, posy, 0);
        Instantiate(birds[birdscoun],pos,birds[birdscoun].transform.rotation);
    }
}
