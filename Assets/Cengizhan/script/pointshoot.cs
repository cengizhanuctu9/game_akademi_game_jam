using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pointshoot : MonoBehaviour
{
    public GameObject coroshair;
    Vector3 taget;
    public GameObject gun;
    public GameObject bullet;
    float bulledspeed = 10;
    AudioSource audiogun;

    private void Start()
    {
        audiogun = GetComponent<AudioSource>();
       Cursor.visible = false;
    }
    private void Update()
    {
        taget = transform.GetComponent<Camera>().ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, transform.position.z));
        coroshair.transform.position = new Vector2(taget.x,taget.y);
        Vector3 difference = taget - gun.transform.position;
        float rotationz = Mathf.Atan2(difference.y, difference.x)*Mathf.Rad2Deg;
        gun.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationz);
        if (Input.GetMouseButtonDown(1))
        {
            float distance = difference.magnitude;
            Vector2 direction = difference / distance;
            direction.Normalize();
            shoot(direction,rotationz);
            audiogun.Play();
        }
    }
    void shoot(Vector2 direction,float rotatez)
    {
        GameObject b = Instantiate(bullet) as GameObject;
        b.transform.position = gun.transform.position;
        b.transform.rotation = Quaternion.Euler(0,0,0);
        b.GetComponent<Rigidbody2D>().velocity = direction*bulledspeed;
    }
}
