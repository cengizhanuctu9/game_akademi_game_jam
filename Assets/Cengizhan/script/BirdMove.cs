using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdMove : MonoBehaviour
{
    public float speed = 1;
    public int movetyp;
   
  
    void Update()
    {
        if (movetyp == 0)
        {
            transform.position += new Vector3(0,1,0)* Time.deltaTime * speed;
        }
        if (movetyp == 1)
        {
            transform.position += new Vector3(1, -0.8f, 0) * Time.deltaTime * speed;
        }
        if (movetyp == 2)
        {
            transform.position += new Vector3(-1, -0.8f, 0) * Time.deltaTime * speed;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("wall"))
        {
            transform.localScale = new Vector3(0.2f, -1*transform.localScale.y, -1);
            speed *= -1;
        }
       

    }
}
