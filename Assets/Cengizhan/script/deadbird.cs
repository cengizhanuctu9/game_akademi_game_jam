using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deadbird : MonoBehaviour
{
    public GameObject deadbirdobj;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("bullet"))
        {
            Instantiate(deadbirdobj, transform.position, deadbirdobj.transform.rotation);
            gameObject.SetActive(false);

        }
    }
}
