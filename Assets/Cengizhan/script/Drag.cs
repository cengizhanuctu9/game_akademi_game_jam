using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag : MonoBehaviour
{
    bool isdrag = false;
    Vector3 mousedragstartpos;
    Vector3 spritedragstartpos;
    public GameObject izgara;
    public int plasgas;

    private void OnMouseDown()
    {
        isdrag = true;
        mousedragstartpos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        spritedragstartpos = transform.localPosition;
       
    }
    private void OnMouseDrag()
    {
        if (isdrag)
        {
            transform.localPosition = spritedragstartpos + (Camera.main.ScreenToWorldPoint(Input.mousePosition) - mousedragstartpos);
        }
    }
    private void OnMouseUp()
    {

        isdrag = false;
        
       
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            
            // yanm�� ku sesi ve g�r�nt�s�
            Instantiate(izgara, transform.position, Quaternion.identity);
            StartCoroutine(speedbackround());
            Gass.gassplas(plasgas);
            GetComponent<Rigidbody2D>().gravityScale = 1;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
      
    }
    IEnumerator speedbackround()
    {
        move.speed = 10;
        yield return new WaitForSeconds(0.4f);
        move.speed = 5;
    }
}
