using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Gass : MonoBehaviour
{
    public Slider slider;
    public GameObject player;
    public static float gass=100;
    public static float score;
    public Text scorevalue;
    bool isscorewrite= true;
    public GameObject gameoverpanel;
    public Text highscoretxt;
    public Text scoregameover;
    public GameObject startpanel;
  
    void Start()
    {
      
        Time.timeScale = 0;
        slider.value = gass;
        PlayerPrefs.SetInt("highscore", 0);
    }

    
    void Update()
    {
       
        if (gass > 100)
        {
            gass = 100;
        }
        gass -= Time.deltaTime * 10;
        slider.value = gass;
        if (gass < 10)
        {
            //dusme sesi
            StartCoroutine(gameover());
            isscorewrite = false;
            player.GetComponent<Rigidbody2D>().gravityScale = 1;

        }
        else
        {
            player.GetComponent<Rigidbody2D>().gravityScale = 0;
        }
        if (isscorewrite)
        {
           
            score += Time.deltaTime * Backround.speed;
            scorevalue.text = Mathf.FloorToInt(score).ToString();
            if (score > PlayerPrefs.GetFloat("highscore"))
            {
                scoregameover.text = score.ToString();
                PlayerPrefs.SetFloat("highscore", score);
                highscoretxt.text = PlayerPrefs.GetFloat("highscore").ToString();

            }
        }
    }
    public static void gassplas(int plasgas)
    {
        gass += plasgas;
    }

    IEnumerator gameover()
    {
        
        yield return new WaitForSeconds(1.5f);
        //Time.timeScale = 0;
        gameoverpanel.SetActive(true);
    }
   public  void startgame()
    {
        
        startpanel.SetActive(false);
        Time.timeScale = 1;
    }
   

}
