using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowAward : MonoBehaviour
{
    Vector2 target;
    Vector2 target2;

    public GameObject TargetPot;
   public GameObject TargetPot2;
    public GameObject hedef;

    [SerializeField]
    bool movement = true;
    [SerializeField]
    float Speed = 5f;
    // Start is called before the first frame update
    void Start()
    {
        TargetPot = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {



        if (movement)
        {
            Quaternion rotation = Quaternion.LookRotation
   (TargetPot.transform.position - transform.position, transform.TransformDirection(Vector3.up));
            transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
            target = TargetPot.transform.position;
            transform.position = Vector2.MoveTowards(transform.position, target, Speed * Time.deltaTime);

            Debug.Log("A");

        }

        if (!movement)
        {
            Vector3 vec = new Vector3(hedef.transform.position.x,hedef.transform.position.y,-hedef.transform.position.z);
            target2 = TargetPot2.transform.position;
            transform.position = Vector2.MoveTowards(transform.position, target2, Speed * Time.deltaTime);

            transform.rotation = Quaternion.Euler(0,0,270);
            hedef.transform.position = vec;
            Quaternion rotation = Quaternion.LookRotation
   (TargetPot2.transform.position - transform.position, transform.TransformDirection(Vector3.up));
            transform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
            Debug.Log("B");

        }
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "catch")
        {
            movement = false;
        }


      

    }
}
